﻿using UnityEngine;

public class ItemManager : MonoBehaviour
{
    /** 此為道具管理腳本，控制遊戲中被玩家撿起來的道具 **/
    
    //裝備類型
    public static ItemTypeEquipment[] ItemEquipmentList;
    //任務類型
    public static ItemTypeTask[] ItemTaskList;
    //消耗類型
    public static ItemTypeCost[] ItemCostList;
}
