﻿public class ItemMagicWand : ItemTypeEquipment
{
    public string Name = "魔杖";
    public int Amount = 1;
    public override string m_name { 
        get { return Name; } 
        set { m_name = Name;  }
    }
    public override int m_amount
    {
        get { return Amount; }
        set { m_amount = Amount; }
    }

    public override void Use()
    {
    }
}
