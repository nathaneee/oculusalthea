﻿using UnityEngine;

public abstract class ItemBase : MonoBehaviour
{
    public abstract string m_name { get; set; }//道具名稱
    public abstract string m_type { get; }//道具類型
    public abstract bool m_droppable { get; }//道具是否可丟棄
    public abstract int m_amount { get; set; }//道具數量

    virtual public void Use() { if (m_amount <= 0) return; m_amount--; }//道具使用的Function
    virtual public void Drop() { if (!m_droppable) return; }//道具丟棄的Function
    virtual public void Update() { }//判斷道具是否使用完畢、是否丟棄，預設為空函數以免消耗資源，需要時再override
}

public abstract class ItemTypeEquipment : ItemBase
{
    public override string m_type { get { return "裝備型"; } }

    public override bool m_droppable { get { return false; } }
}
public abstract class ItemTypeTask : ItemBase
{
    public override string m_type { get { return "任務型"; } }
    public override bool m_droppable { get { return false; } }
}
public abstract class ItemTypeCost : ItemBase
{
    public override string m_type { get { return "消耗型"; } }
    public override bool m_droppable { get { return true; } }
}