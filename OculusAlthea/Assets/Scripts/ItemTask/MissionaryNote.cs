﻿public class MissionaryNote : ItemTypeTask
{
    public string Name = "傳教士筆記";
    public int Amount = 1;
    public override string m_name
    {
        get { return Name; }
        set { m_name = Name; }
    }
    public override int m_amount
    {
        get { return Amount; }
        set { m_amount = Amount; }
    }

    public override void Use()
    {
        base.Use();
    }
    public override void Drop()
    {
        base.Drop();
    }
    public override void Update()
    {
        //if (m_amount <= 0) Destroy(gameObject.transform.parent.gameObject);
    }
}
